﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardController : MonoBehaviour
{

    public int columns;
    public int rows;

    public GameObject[] Floors;
    public GameObject[] OuterWalls;
    public GameObject[] FoodItems;
    public GameObject[] Walls;
    public GameObject[] Enemies;

    public GameObject Exit;

    private Transform gameBoard;
    private List<Vector3> obstaclesGrid;

    void Awake()
    {
        obstaclesGrid = new List<Vector3>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void InitializeObstaclePositions()
    {
        obstaclesGrid.Clear();

        for (int x = 2; x < columns - 2; x++)
        {

            for (int y = 2; y < rows - 2; y++)
            {
                obstaclesGrid.Add(new Vector3(x, y, 0f));
            }

        }
    }


    private void SetupGameBoard()
    {
        gameBoard = new GameObject("game Board").transform;

        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                GameObject selectedTile;
                if (x == 0 || y == 0 || x == columns - 1 || y == rows - 1)
                {
                    selectedTile = OuterWalls[Random.Range(0, OuterWalls.Length)];
                }
                else
                {
                    selectedTile = Floors[Random.Range(0, Floors.Length)];
                }
                GameObject floorTile = (GameObject)Instantiate(selectedTile, new Vector3(x, y, 0f), Quaternion.identity);
                floorTile.transform.SetParent(gameBoard);
            }
        }
    }

    private void SetRandomObstaclesOnGrid(GameObject[] obstaclesArray, int minimum, int maximum)
    {
        int obstacleCount = Random.Range(minimum, maximum + 1);

        if (obstacleCount > obstaclesGrid.Count)
        {
            obstacleCount = obstaclesGrid.Count;
        }
        for (int index = 0; index < obstacleCount; index++)
        {
            GameObject selectedObstacle = obstaclesArray[Random.Range(0, obstaclesArray.Length)];
            Instantiate(selectedObstacle, SelectGridPostion(), Quaternion.identity);

        }
    }

    private Vector3 SelectGridPostion()
    {
        int randomIndex = Random.Range(0, obstaclesGrid.Count);
        Vector3 randomPosition = obstaclesGrid[randomIndex];
        obstaclesGrid.RemoveAt(randomIndex);
        return randomPosition;
    }



    public void SetupLevel(int currentLevel)
    {
        InitializeObstaclePositions();
        SetupGameBoard();
        SetRandomObstaclesOnGrid(Walls, 3, 9);
        SetRandomObstaclesOnGrid(FoodItems, 1, 5);
        int enemyCount = (int)Mathf.Log(currentLevel, 3);
        SetRandomObstaclesOnGrid(Enemies, 1, 3);
        Instantiate(Exit, new Vector3(columns - 2, rows - 2, 0f), Quaternion.identity);
    }
}
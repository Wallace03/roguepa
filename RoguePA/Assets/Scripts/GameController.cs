﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;

    private BoardController BoardController;
    private List<Enemy> enemies;
    private GameObject LevelImage;
    public Text LevelText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;

    void Awake()
    {

        if (Instance != null && Instance != this)

        {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        BoardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();




    }
    void Start()
    {
        InitializeGame();

    }

    private void InitializeGame()
    {
        settingUpGame = true;
        LevelImage = GameObject.Find("Level Image");
        LevelText = GameObject.Find("Level Text").GetComponent<Text>();
        LevelText.text = "Day" + currentLevel;
        LevelImage.SetActive(true);
        enemies.Clear();
        BoardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
    }


    private void DisableLevelImage()
    {
        LevelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }


    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }

    void Update()
    {

        if (isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }
        StartCoroutine(MoveEnemies());
    }

    private void StartCoroutine(IEnumerable enumerable)
    {

    }

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach (Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;


    }
    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void GameOver()
    {
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        LevelText.text = "You Starved after" + currentLevel + "days...";
        LevelImage.SetActive(true);
        enabled = false;
    }
}
